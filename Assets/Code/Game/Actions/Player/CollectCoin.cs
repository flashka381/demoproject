using Code.Core.Actions;
using Code.Core.Attributes.Common;

namespace Code.Game.Actions.Player
{
    public class CollectCoin : BaseAction<CollectableAttribute>
    {
        protected override bool Action(CollectableAttribute data)
        {
            data.Collect();

            return true;
        }
    }
}
