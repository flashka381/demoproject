using Code.Core.ScriptableORM.Abstraction;
using Code.Core.ScriptableORM.Services;
using UnityEngine;
using UnityEngine.Events;

namespace Code.Core.ScriptableORM
{
    public class FileScriptableStorage<T> : StorableScriptableObject where T : Model, new()
    {
        [Header("Data")]
        [SerializeField] private T data;

        [Header("Events")]
        [HideInInspector] public UnityEvent onSave;
        [HideInInspector] public UnityEvent onLoad;
        [HideInInspector] public UnityEvent onSetData;
        
        public T Data
        {
            get => data;
            set
            {
                data = value;
                onSetData?.Invoke();
            }
        }

        public override bool Save()
        {
            if (!FileStorageService<T>.Save(name, data))
                return false;
            
            onSave.Invoke();
            
            Debug.Log("Save:" + name);
            
            return true;
        }

        public override bool Load()
        {
            if (!FileStorageService<T>.Load(name, out var loadedData))
                return false;

            data = loadedData;
            
            onLoad?.Invoke();
            
            return true;
        }

    }
}
