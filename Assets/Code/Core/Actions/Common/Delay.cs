using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Code.Core.Actions.Common
{
    public class Delay : BaseAction
    {
        [Header("Settings")]
        public float delay;
        public float addRandomValue;
        public bool useUnscaledTime = false;

        [Header("Events")]
        public UnityEvent onTimeElapsed;

        private bool _triggered = false;

        protected override bool Action()
        {
            if (_triggered)
                return false;
            
            _triggered = true;
            
            StartCoroutine(DelayInSeconds());
            
            return true;
        }

        private IEnumerator DelayInSeconds()
        {
            if (useUnscaledTime)
                yield return new WaitForSecondsRealtime(delay + Random.Range(0, addRandomValue));
            else
                yield return new WaitForSeconds(delay + Random.Range(0, addRandomValue));
            
            _triggered = false;
            
            onTimeElapsed.Invoke();
        }
    }
}
