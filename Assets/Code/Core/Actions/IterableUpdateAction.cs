namespace Code.Core.Actions
{
    public abstract class IterableUpdateAction : IterableAction
    {
        private void Update()
        {
            if (completed)
                return;

            if (!active)
                return;

            if (!Iterate())
            {
                StopAction();
                
                completed = true;
                
                onComplete.Invoke();
            }
        }
    }
}
