using Code.Core.Actions;
using Code.Core.Attributes.Common;
using Code.Core.Extensions;
using Code.Game.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Code.Game.Actions.Conditions
{
    public class CheckIfTriggerWithCollectable : BaseAction<Collider>
    {
        [Header("Events")]
        public UnityEventWithCollectable onTrue;
        public UnityEvent onFalse;
        
        protected override bool Action(Collider data)
        {
            if (data.gameObject.TryGetAttribute<CollectableAttribute>(out var collectableAttribute))
            {
                onTrue?.Invoke(collectableAttribute);
                return true;
            }
            
            onFalse?.Invoke();
            return false;
        }
    }
}
