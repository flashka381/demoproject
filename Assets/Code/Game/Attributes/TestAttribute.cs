using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Code.Game.Attributes
{
    public class TestAttribute : MonoBehaviour
    {
        public PlayerInput playerInput;

        private void OnEnable()
        {
            playerInput = new PlayerInput();
            playerInput.Enable();
            playerInput.Player.Move.performed += MoveOnPerformed;
        }

        private void MoveOnPerformed(InputAction.CallbackContext obj)
        {
            Debug.Log("TEST");
        }
    }
}
