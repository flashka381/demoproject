using Code.Core.Actions;
using Code.Game.Attributes;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Code.Game.Actions.Player
{
    public class SetMoveDirection : BaseAction<InputAction.CallbackContext>
    {
        [Header("Attributes")]
        public PlayerMovementAttribute playerMovementAttribute;

        protected override bool Action(InputAction.CallbackContext data)
        {
            var value = data.ReadValue<Vector2>();

            playerMovementAttribute.direction = value;

            return true;
        }
    }
}
