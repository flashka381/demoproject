using UnityEngine;

namespace Code.Core.ScriptableORM
{
    [System.Serializable]
    public class Model
    {
        public Model Clone<TModel>() where TModel : Model, new()
        {
            string json = JsonUtility.ToJson(this);
            Model clone = new TModel();
            JsonUtility.FromJsonOverwrite(json, clone);

            return clone;
        }
    }
}
