using Code.Core.Actions;
using Code.Game.Data.ScriptableObjects;
using TMPro;
using UnityEngine;

namespace Code.Game.Actions.UI
{
    public class UpdateCurrentSpeed : BaseAction
    {
        [Header("Data")]
        public PlayerStats playerStats;
        public TMP_Text currentSpeedTextUi;
        
        protected override bool Action()
        {
            if (!playerStats || !currentSpeedTextUi)
            {
                return false;
            }

            currentSpeedTextUi.text = "Current speed: " + playerStats.Data.moveSpeed;
            
            return true;
        }
    }
}
