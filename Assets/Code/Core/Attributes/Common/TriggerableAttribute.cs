using UnityEngine;
using UnityEngine.Events;

namespace Code.Core.Attributes.Common
{
    [System.Serializable]
    public class UnityEventCollider : UnityEvent<Collider>
    {
        
    }
    
    
    public class TriggerableAttribute : BaseAttribute
    {
        [Header("State")]
        [SerializeField] private bool active = true;
        public bool Active
        {
            get => active;
            set => active = value;
        }

        [Header("Events")]
        public UnityEventCollider onTriggerEnter;
        public UnityEventCollider onTriggerStay;
        public UnityEventCollider onTriggerExit;

        public Collider Collider { get; private set; }

        private void OnTriggerEnter(Collider other)
        {
            if (!active)
                return;
            
            Collider = other;
            
            onTriggerEnter.Invoke(other);
        }

        private void OnTriggerStay(Collider other)
        {
            if (!active)
                return;
            
            Collider = other;
            
            onTriggerStay.Invoke(other);
        }

        private void OnTriggerExit(Collider other)
        {
            if (!active)
                return;
            
            Collider = other;
            
            onTriggerExit.Invoke(other);
        }
    }
}