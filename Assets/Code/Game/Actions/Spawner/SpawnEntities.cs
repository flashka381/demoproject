using Code.Core.Actions;
using Code.Game.Attributes;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Code.Game.Actions.Spawner
{
    public class SpawnEntities : BaseAction
    {
        [Header("Attributes")]
        public CollectableSpawnerAttribute spawnerAttribute;
        
        private Vector2 _xSpawnBorders;
        private Vector2 _ySpawnBorders;

        private Vector3 _spawnerPosition;
        
        protected override bool Action()
        {
            if (!spawnerAttribute)
            {
                Debug.LogError("SpawnAttribute is empty", this);
                return false;
            }
            
            _spawnerPosition = spawnerAttribute.transform.position;
            
            for (var i = spawnerAttribute.currentEntitiesCount; i < spawnerAttribute.maxEntities; i++)
            {
                var temp = SpawnEntity(spawnerAttribute.spawnObject.gameObject, spawnerAttribute.spawnRootObject);
                spawnerAttribute.RegisterEntity(temp);
            }
            
            spawnerAttribute.onAllEntitySpawned?.Invoke();
            
            return true;
        }

        private GameObject SpawnEntity(GameObject prefab, Transform root)
        {
            var spawnPosition = new Vector3(
                Random.Range(_spawnerPosition.x - spawnerAttribute.spawnArea.x / 2, _spawnerPosition.x + spawnerAttribute.spawnArea.x / 2),
                0,
                Random.Range(_spawnerPosition.y - spawnerAttribute.spawnArea.y / 2, _spawnerPosition.y + spawnerAttribute.spawnArea.y / 2));

            return Instantiate(prefab, spawnPosition, Quaternion.identity, root);
        }
    }
}
