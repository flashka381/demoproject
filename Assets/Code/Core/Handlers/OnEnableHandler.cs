using UnityEngine;
using UnityEngine.Events;

namespace Code.Core.Handlers
{
    public class OnEnableHandler : MonoBehaviour
    {
        [Header("Events")]
        public UnityEvent onEnable;

        private void OnEnable()
        {
            onEnable?.Invoke();
        }
    }
}
