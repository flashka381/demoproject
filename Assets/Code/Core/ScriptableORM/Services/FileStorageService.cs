using System.IO;
using UnityEngine;

namespace Code.Core.ScriptableORM.Services
{
    public static class FileStorageService<T> where T : Model, new()
    {
        private const string FILE_EXTENTION = ".json";
        
        private static  string GetStoragePath(string name) =>
            Application.persistentDataPath + Path.DirectorySeparatorChar + name + FILE_EXTENTION;

        public static bool Save(string name, T model)
        {
            var json = JsonUtility.ToJson(model);
            File.WriteAllText(GetStoragePath(name), json);
            
            return true;
        }
        
        public static bool Load(string name, out T model)
        {
            model = new T();
            
            if (!File.Exists(GetStoragePath(name)))
                return false;

            var json = File.ReadAllText(GetStoragePath(name));
            JsonUtility.FromJsonOverwrite(json, model);
            
            return true;
        }
    }
}
