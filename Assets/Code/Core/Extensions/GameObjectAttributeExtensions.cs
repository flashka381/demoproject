using Code.Core.Attributes;
using JetBrains.Annotations;
using UnityEngine;

namespace Code.Core.Extensions
{
    public static class GameObjectAttributeExtensions
    {
        public static bool HasAttribute<T>(this GameObject instance) where T : BaseAttribute
        {
            return instance.GetComponent<T>();
        }
        
        [CanBeNull]
        public static T GetAttribute<T>(this GameObject instance) where T : BaseAttribute
        {
            return instance.GetComponent<T>();
        }

        public static bool TryGetAttribute<T>(this GameObject instance, out T value) where T : BaseAttribute
        {
            var attribute = instance.GetComponent<T>();
            value = attribute;
            
            return attribute != null;
        }
    }
}
