using Code.Core.Attributes;
using Code.Core.Attributes.Common;
using Code.Game.Data.ScriptableObjects;
using UnityEngine;

namespace Code.Game.Attributes
{
    public class PlayerMovementAttribute : BaseAttribute
    {
        [Header("Data")]
        public Vector2 direction;
        
        [Header("Components")]
        public RigidbodyAttribute rigidbodyAttribute;
        public PlayerStats playerStats;

    }
}
