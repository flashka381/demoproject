using System.Collections.Generic;
using Code.Core.Attributes;
using Code.Core.Attributes.Common;
using Code.Core.Extensions;
using Code.Game.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Code.Game.Attributes
{
    public class CollectableSpawnerAttribute : BaseAttribute
    {
        [Header("Settings")]
        public int maxEntities;
        public Vector2 spawnArea;
        public CollectableAttribute spawnObject;
        public Transform spawnRootObject;

        [Header("Data")]
        public int currentEntitiesCount;
        
        [Header("Events")]
        public UnityEvent onAllEntitySpawned;
        public UnityEventWithCollectable onEntityCollected;

        public void RegisterEntity(GameObject entity)
        {
            if (entity.TryGetAttribute<CollectableAttribute>(out var collectableAttribute))
            {
                currentEntitiesCount++;
                collectableAttribute.onCollected.AddListener(EntityCollectedHandler);
            }
        }

        private void EntityCollectedHandler(CollectableAttribute collectableAttribute)
        {
            currentEntitiesCount--;
            onEntityCollected?.Invoke(collectableAttribute);
        }
        
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(transform.position, new Vector3(spawnArea.x, 1, spawnArea.y));
        }
    }
}
