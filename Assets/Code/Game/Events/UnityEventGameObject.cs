using UnityEngine;
using UnityEngine.Events;

namespace Code.Game.Events
{
    [System.Serializable]
    public class UnityEventGameObject : UnityEvent<GameObject>
    {

    }
}
