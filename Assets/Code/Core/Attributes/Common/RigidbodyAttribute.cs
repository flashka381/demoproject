using UnityEngine;

namespace Code.Core.Attributes.Common
{
    [RequireComponent(typeof(Rigidbody))]
    public class RigidbodyAttribute : BaseAttribute
    {
        // ReSharper disable once MemberCanBePrivate.Global
        public Rigidbody Rigidbody { get; private set; }
        
        private void Awake()
        {
            Rigidbody = GetComponent<Rigidbody>();
        }
    }
}
