using UnityEngine;
using UnityEngine.Events;

namespace Code.Core.Attributes.Common
{
    [System.Serializable]
    public class UnityEventCollision : UnityEvent<Collision>
    {
        
    }
    
    public class CollidableAttribute : BaseAttribute
    {
        [Header("State")]
        [SerializeField] private bool active = true;
        public bool Active
        {
            get => active;
            set => active = value;
        }

        [Header("Events")]
        public UnityEventCollision onCollisionEnter;
        public UnityEventCollision onCollisionStay;
        public UnityEventCollision onCollisionExit;

        public Collision Collision { get; private set; }

        private Rigidbody _rigidbody;
        public Rigidbody Rigidbody
        {
            get => _rigidbody;
            private set { _rigidbody = value;  }
        }

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void OnCollisionEnter(Collision other)
        {
            if (!active)
                return;
            
            // Debug.Log("Collision");
            
            Collision = other;
            
            onCollisionEnter.Invoke(other);
        }

        private void OnCollisionStay(Collision other)
        {
            if (!active)
                return;
            
            Collision = other;
            
            onCollisionStay.Invoke(other);
        }

        private void OnCollisionExit(Collision other)
        {
            if (!active)
                return;
            
            Collision = other;
            
            onCollisionExit.Invoke(other);
        }
    }
}