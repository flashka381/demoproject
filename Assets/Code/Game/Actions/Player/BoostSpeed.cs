using Code.Core.Actions;
using Code.Game.Data.ScriptableObjects;
using UnityEngine;

namespace Code.Game.Actions.Player
{
    public class BoostSpeed : BaseAction
    {
        [Header("Settings")]
        public int boostValue;
        
        [Header("Data")]
        public PlayerStats playerStats;
        
        protected override bool Action()
        {
            if (!playerStats)
            {
                Debug.LogError("PlayerStats is empty");
                return false;
            }

            playerStats.Data.moveSpeed += boostValue;
            playerStats.Save();
            
            return true;
        }
    }
}
