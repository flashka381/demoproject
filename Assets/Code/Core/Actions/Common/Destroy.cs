using Code.Core.Attributes.Common;
using UnityEngine;

namespace Code.Core.Actions.Common
{
    public class Destroy : BaseAction
    {
        [Header("Attributes")]
        public DestroyableAttribute destroyableAttribute;
        
        [Header("Settings")]
        public bool destroyImmediate;
        
        protected override bool Action()
        {
            if (destroyImmediate)
            {
                DestroyImmediate(destroyableAttribute.gameObject);
            }
            else
            {
                Destroy(destroyableAttribute.gameObject);
            }
            
            return true;
        }
    }
}
