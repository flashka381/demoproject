using UnityEngine;
using UnityEngine.Events;

namespace Code.Core.Actions
{
    public abstract class IterableAction : MonoBehaviour
    {
        [Header("Events")]
        public UnityEvent onStart = new UnityEvent();
        public UnityEvent onStop = new UnityEvent();
        public UnityEvent onComplete = new UnityEvent();

        protected bool completed = true;
        protected bool active = false;

        public void StartAction()
        { 
            completed = false;
            active = true;
            
            onStart.Invoke();
        }

        public void StopAction()
        {
            active = false;
            
            onStop.Invoke();
        }

        protected abstract bool Iterate();
    }
}