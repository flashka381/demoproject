using Code.Core.ScriptableORM.Abstraction;
using UnityEditor;
using UnityEngine;

namespace Code.Core.ScriptableORM.Editor
{
    [CustomEditor(typeof(StorableScriptableObject), true)]
    public class StorableEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            // if(((StorableScriptableObject) target).storable == false)
            //     return;
            
            GUILayout.Space(40);

            if (GUILayout.Button("Save", GUILayout.Height(40)))
            {
                ((StorableScriptableObject) target).Save();
            }

            if (GUILayout.Button("Load", GUILayout.Height(40)))
            {
                ((StorableScriptableObject) target).Load();
            }

        }
    }
}
