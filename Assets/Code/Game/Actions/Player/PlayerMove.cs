using Code.Core.Actions;
using Code.Game.Attributes;
using UnityEngine;

namespace Code.Game.Actions.Player
{
    public class PlayerMove : IterableUpdateAction
    {
        [Header("Data")]
        public PlayerMovementAttribute playerMovementAttribute;
        
        protected override bool Iterate()
        {
            var joystickInput =
                new Vector3(playerMovementAttribute.direction.x, 0, playerMovementAttribute.direction.y);
            
            playerMovementAttribute.transform.Translate(
                joystickInput * (playerMovementAttribute.playerStats.Data.moveSpeed * Time.deltaTime), Space.World);
            
            return true;
        }
    }
}
