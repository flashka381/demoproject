using Code.Core.ScriptableORM;

namespace Code.Game.Data.Models
{
    [System.Serializable]
    public class PlayerStatsModel : Model
    {
        public float moveSpeed;
    }
}
