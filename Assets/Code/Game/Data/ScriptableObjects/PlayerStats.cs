using Code.Core.ScriptableORM;
using Code.Game.Data.Models;
using UnityEngine;

namespace Code.Game.Data.ScriptableObjects
{
    [CreateAssetMenu(menuName = "Scriptable/PlayerStats", fileName = "PlayerStats")]
    public class PlayerStats : FileScriptableStorage<PlayerStatsModel>
    {

    }
}
