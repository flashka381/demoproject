using UnityEngine;
using UnityEngine.Events;

namespace Code.Core.Actions
{
    public abstract class BaseAction : MonoBehaviour
    {
        [Header("Base Event")]
        public UnityEvent onAction = new UnityEvent();

        public void DoAction()
        {
            if (Action())
            {
                onAction.Invoke();
            }
        }

        protected abstract bool Action();
    }
    
    public abstract class BaseAction<T> : MonoBehaviour
    {
        [Header("Base Event")]
        public UnityEvent onAction = new UnityEvent();

        public void DoAction(T data)
        {
            if (Action(data))
            {
                onAction.Invoke();
            }
        }

        protected abstract bool Action(T data);
    }
}