namespace Code.Core.Actions
{
    public abstract class IterableFixedUpdateAction : IterableAction
    {
        private void FixedUpdate()
        {
            if (completed)
                return;
            
            if (!active)
                return;

            if (!Iterate())
            {
                StopAction();

                completed = true;
                
                onComplete.Invoke();
            }
        }
    }
}