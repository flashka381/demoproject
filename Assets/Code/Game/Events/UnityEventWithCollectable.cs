﻿using Code.Core.Attributes.Common;
using UnityEngine.Events;

namespace Code.Game.Events
{
    [System.Serializable]
    public class UnityEventWithCollectable : UnityEvent<CollectableAttribute>
    {
        
    }
}