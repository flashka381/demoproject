using UnityEngine;

namespace Code.Core.ScriptableORM.Abstraction
{
    public abstract class StorableScriptableObject : ScriptableObject
    {
        public abstract bool Save();

        public abstract bool Load();
    }
}
