using UnityEngine;
using UnityEngine.Events;

namespace Code.Core.Handlers
{
    public class OnStartHandler : MonoBehaviour
    {
        [Header("Events")]
        public UnityEvent onStart;
        
        private void Start()
        {
            onStart?.Invoke();
        }
    }
}
