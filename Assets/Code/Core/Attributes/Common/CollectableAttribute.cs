using Code.Game.Events;
using UnityEngine;

namespace Code.Core.Attributes.Common
{
    public class CollectableAttribute : BaseAttribute
    {
        [Header("Settings")]
        [SerializeField] private bool collectOnce = false;

        private bool _isCollected = false;
        
        [Header("Events")]
        public UnityEventWithCollectable onCollected;

        public void Collect()
        {
            if(collectOnce && _isCollected)
                return;
            
            onCollected?.Invoke(this);
            _isCollected = true;
        }
    }
}
